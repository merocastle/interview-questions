/* 
 * File:   sequence.cpp
 * Author: shikhar subedi
 * 
 * Program to find the element which is out of order in an arithmetic sequence
 * 
 * e.g in the arithmetic sequence 1 2 3 4 5 7 : the element 7 is out of order
 *  
 * input 1: length of the sequence
 * 
 * input 2: the sequence of elements separated by a single space e.g 
 * 
 * 
 * restrictions :  one and only one element can be out of order
 * 
 * restrictions: the length of the arithmetic sequence must be equal to 
 * or greater than 4
 * 
 * 
 *
 * Created on November 27, 2013, 12:50 PM
 */

#include <cstdlib>
#include <iostream>
#include <vector>
#include <sstream>


/**
 * 
 * @param a: the sorted array of integers representing the sequence
 * @param len :the length of the sequence
 * @param diff : the common difference of the sequence
 * @return : the element that is out of order in the arithmetic sequence
 */
int calc_outOrder(int *a, int len, int diff)
{
    int last_diff = (int)((a[len-1] - a[0])/(len-1));
    
    
    //check if either last or first element is out of order
    if(last_diff!= diff)
    {
      int diff10   = a[1] - a[0];
        
        if(diff10 != diff)
            return a[0];
        else 
            return a[len-1];
    }
    //the first and last elements in order. check other elements
    
    int index = 1;
    int first =a[0];
    int next = 0;
    while(true)
    {
        next = first + index*diff;
        if(next!=a[index])
        {
            return a[index];
        }
        index++;
    }
}
/**
 * 
 * @param s : input string representing the arithmetic sequence from the user
 * @param delim : the delimiter separating the elements in the sequence which is a single space
 * @param elems : string vector passed by reference which is the result of the split function
 */
void split(const std::string &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    
}

//function converts the string vector 'elems' into an array
//of sorted integers 'a' of length 'len'
void convert_array(std::vector<std::string> &elems, int *a, int len){
    
    int index = 0;
    
    for(std::vector<std::string>::iterator it = elems.begin();it !=elems.end();it = it+1)
    {
      std::stringstream(*it)>>a[index] ;
      index++;
    }
    
    //now sort the array using insertion sort;
    //note for efficiency other sorting methods can be applied
    
    for(int i = 0 ;i< len ; i++)
    {
         int j = i;
        while(j>0 && a[j]<a[j-1])
        {
            int temp = a[j];
            a[j]= a[j-1];
            a[j-1] = temp;
            
            j--;
        }
    }
    
    
}

//this function calculates the common difference of the arithmetic sequence 'a';
int cal_diff(int *a,int length){
    
   
    std::vector<int> diff;
    
    
    for(int i = 0 ;i <length-1 ;i++)
    {
        diff.push_back((a[i+1]-a[i]));
        
        unsigned size = diff.size();
        
        if(size>=3)
        {
            
            for(unsigned j =0 ; j< size; j++)
            {
                for(unsigned k = j+1 ; k <size; k++)
                {
                    if(diff[j]==diff[k])
                    {
                        std::cout<<std::endl<<"The common difference is "<<diff[j]<<std::endl;
                        return diff[j];
                    }
                }
                
                
            }
            
      
        }
      
        
    }
     //when it comes out here, it means the length of the sequence is 4
       //and the case is like 1 3 6 7 ( 1 3 5 7) or 2 3 6 8 (2 4 6 8) 
    
    int last4 =(int) (a[3] - a[0])/3;
    
    std::cout<<std::endl<<"The common difference is "<<last4<<std::endl;
    return last4;
}

int main(int argc, char** argv) {
    
    std::vector<std::string> sequence_vector; //vector to hold each of the numbers in string format
    
    int length; //length of the sequence must be greater than 3
    
    int common_diff ; //common difference;
    
    std::string sequence_string,length_string;
    
    char delim = ' ';

    std::cout<<"Enter the length of the sequence " ;
   
    std::getline(std::cin,length_string);
   
    std::stringstream(length_string)>>length ;
   
    std::cout<<std::endl<<"Enter the sequence in a line separated by single space";
    std::getline(std::cin, sequence_string);
    
     split(sequence_string,delim,sequence_vector);
    
    int *sequence_int = new int[length];
    
    //convert the vector string into a sorted array of integers
    convert_array(sequence_vector,sequence_int,length);
    
    //calculate the common difference of the sequence;
     common_diff = cal_diff(sequence_int,length);
     
   //find the element not in order;
     
     int out_of_order = calc_outOrder(sequence_int,length, common_diff);
     
     std::cout<<"\n the element out of order is "<<out_of_order;
     
     
     return 0;
    
}

